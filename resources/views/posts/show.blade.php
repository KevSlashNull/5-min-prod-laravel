@extends('layouts.main')

@section('content')
    
<div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
    <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1">
            <div class="p-6">
                <div class="flex items-center">
                    <div class="text-lg leading-7 font-semibold text-gray-900 dark:text-white">{{ $post->title }}</div>
                </div>

                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                    {{ $post->body }}
                </div>
            </div>
        </div>
    </div>

    <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
        <div class="text-center text-sm text-gray-500 sm:text-left">
            <div class="flex items-center">
                <a href="/posts" class="ml-1 underline">
                    Back to all posts
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
