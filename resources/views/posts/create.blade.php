@extends('layouts.main')

@section('content')
<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="grid grid-cols-1">
        <div class="p-6">
            <div class="flex items-center">
                <div class="text-lg leading-7 font-semibold text-gray-900 dark:text-white">New Post</div>
            </div>

            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                <form action="{{ route('posts.store') }}" method="post">
                    @csrf
                    <div>
                        <label class="block" for="title">Title</label>
                        <input type="text" name="title" id="title">
                    </div>
                    <div>
                        <label class="block" for="body">Body</label>
                        <textarea name="body" id="body" cols="30" rows="4"></textarea>
                    </div>
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
