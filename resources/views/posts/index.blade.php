@extends('layouts.main')

@section('content')
<div class="flex" style="flex-direction: column">
    @foreach ($posts as $post)
    <div class="p-2">
        <div class="text-lg leading-7 font-semibold"><a href="{{ route('posts.show', $post->id) }}" class="underline text-gray-900 dark:text-white">{{ $post->title }}</a></div>
    </div>
    @endforeach

    <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
        <div class="text-center text-sm text-gray-500 sm:text-left">
            <div class="flex items-center">
                <a href="{{ url('/') }}" class="ml-1 underline">
                   Home
                </a>
                <span class="ml-1">
                &middot;
                </span>
                <a href="{{ route('posts.create') }}" class="ml-1 underline">
                   Create a post
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
